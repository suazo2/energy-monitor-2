import logging
import os
import subprocess
import sys
import timeit
from datetime import datetime
from math import sqrt
from prettytable import PrettyTable
import spidev

from utils import load_config

logger = logging.getLogger('energy_monitor')
logger.setLevel(logging.INFO)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s : %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
ch_formatter = logging.Formatter('%(levelname)s : %(message)s')
ch.setFormatter(ch_formatter)
logger.addHandler(ch)


class EnergyMonitor:
    """ Collects raw readings from RP """

    def __init__(self):
        # Check for running instances
        self.pid = os.getpid()
        self.duplicate_instance_check()

        # Load config file
        self.config = load_config()
        self.grid_voltage = self.config.get('grid_voltage').get('grid_voltage')
        self.ac_transformer_output_voltage = self.config.get('grid_voltage').get('ac_transformer_output_voltage')
        self.voltage_calibration = self.config.get('grid_voltage').get('voltage_calibration')
        self.enabled_channels = None
        self.enabled_adc_ct_channels = None
        self.mains_channels = None
        self.production_channels = None
        self.consumption_channels = None

        self.configure_channels()

        # Spidev setup (serial communication)
        self.spi = spidev.SpiDev()
        self.spi.open(0, 0)
        self.spi.max_speed_hz = 1750000

    def duplicate_instance_check(self):
        # Check to see if there is already a power monitor process running.
        logger.debug("  ..Checking to see if the power monitor is already running or not...")
        c = subprocess.run('sudo systemctl status power-monitor.service | grep "Main PID"', shell=True,
                           capture_output=True)
        output = c.stdout.decode('utf-8').lower()
        if str(self.pid) not in output and len(output) > 10 and 'code=' not in output:
            logger.warning("Application already running in the background")
            self.cleanup()

        # Check process list in case user is running the power monitor manually in an SSH session.
        c = subprocess.run('sudo ps -aux | grep "power_monitor.py" | grep -v "grep"', shell=True, capture_output=True)
        output = c.stdout.decode('utf-8').lower()
        if len(output.splitlines()) > 1:
            if 'power_monitor.py' in c.stdout.decode('utf-8').lower():
                for _ in range(6):
                    output = output.replace('  ', ' ')
                output = output.split(' ')
                user, pid = output[0], output[1]
                logger.warning(
                    f"It appears that the user {user} is already running the power monitor in another session (process ID {pid}). You should not run two copies at the same time because they will compete with each other for access to the PCB.")
                logger.warning(
                    f"If you're not sure where or how the other power monitor session is running, you can kill it with the following command: kill -9 {pid}")
                self.cleanup()

    def get_channels(self, enabled=None, channel_type=None):
        channels = [
            int(channel.split('_')[-1])
            for channel, settings in self.config['current_transformers'].items()
            if (enabled is None or settings['enabled'] == True) and
               (channel_type is None or settings['type'] == channel_type)
        ]
        return channels

    def configure_channels(self):
        invalid_settings = False

        # Enabled Channels
        self.enabled_channels = self.get_channels(enabled=True)
        if not self.enabled_channels:
            invalid_settings = True
            logger.warning("Invalid config file setting: No channels have been enabled!")
        else:
            logger.debug(f"Sampling enabled for {len(self.enabled_channels)} channels.")

        ADC_CHANNELS = {
            1: 0,  # PCB channel # : ADC channel #
            2: 1,
            3: 2,
            4: 3,
            5: 6,
            6: 7
        }
        self.enabled_adc_ct_channels = {pcb_chan: adc_chan for pcb_chan, adc_chan in ADC_CHANNELS.items() if
                                        pcb_chan in self.enabled_channels}

        # CT Type Check
        for ct_channel, settings in self.config['current_transformers'].items():
            if settings['type'] not in ('consumption', 'production'):
                logger.warning(
                    f"Invalid config file setting: 'type' for {ct_channel} should be 'consumption' or 'production''. It is currently set to: '{self.config['current_transformers'][ct_channel]['type']}'.")
                invalid_settings = True

        if invalid_settings:
            logger.critical(
                "Invalid settings detected in config.toml. Please review any warning messages above and correct the "
                "issue.")
            self.cleanup()

        # Production sources assignment
        self.production_channels = self.get_channels(enabled=True, channel_type='production')
        logger.debug(f"Identified {len(self.production_channels)} production channels: ({self.production_channels})")

        # Consumption sources assignment
        self.consumption_channels = self.get_channels(enabled=True, channel_type='consumption')
        logger.debug(f"Identified {len(self.consumption_channels)} consumption channels: ({self.consumption_channels})")

    def get_board_voltage(self):
        """ Take 10 sample readings and return the average board voltage from the +3.3V rail. """
        samples = []
        while len(samples) <= 10:
            data = self.read_adc(4)  # channel 4 is the 3.3V ref voltage
            samples.append(data)

        avg_reading = sum(samples) / len(samples)
        board_voltage = (avg_reading / 1024) * 3.31 * 2
        return board_voltage

    def read_adc(self, adc_num):
        """ Read SPI data from the MCP3008, 8 channels in total. """
        r = self.spi.xfer2([1, 8 + adc_num << 4, 0])
        data = ((r[1] & 3) << 8) + r[2]
        return data

    def collect_data(self, num_samples):
        """
        Reads data from the Analog to Digital Converter for each channel
        """

        now = datetime.utcnow()
        samples = {}

        # Setup header for each active channel
        for pcb_chan in self.enabled_channels:
            samples[f'ct{pcb_chan}'] = []
            samples[f'v{pcb_chan}'] = []

        # Time sample duration
        start = timeit.default_timer()

        # In each sample, save the reading from the ADC
        for _ in range(num_samples):
            for pcb_chan, adc_chan in self.enabled_adc_ct_channels.items():
                samples[f'ct{pcb_chan}'].append(self.read_adc(adc_chan))
                samples[f'v{pcb_chan}'].append(self.read_adc(adc_chan))
        stop = timeit.default_timer()
        duration = stop - start

        samples['time'] = now
        samples['duration'] = duration

        return samples

    def calculate_power(self, samples, board_voltage):
        """ Calculates amperage, real power, power factor, and voltage"""

        # current samples
        ct_samples = [samples.get(f'ct{idx}') for idx in range(1,7)]
        # voltage wave
        v_samples = [samples.get(f'v{idx}') for idx in range(1,7)]

        # Variable Initialization
        sum_inst_power = [0] * 6
        sum_squared_current = [0] * 6
        sum_raw_current = [0] * 6
        sum_squared_voltage = [0] * 6
        sum_raw_voltage = [0] * 6

        # Scaling factors
        vref = board_voltage / 1024
        ct_scaling_factors = []
        for chan_num in range(1, 7):
            ct_scaling_factors.append(
                vref * self.config['current_transformers'][f'channel_{chan_num}']['calibration'] * int(
                    self.config['current_transformers'][f'channel_{chan_num}']['rating']) * self.def_cal)
        ac_voltage_ratio = (self.grid_voltage / self.ac_transformer_output_voltage) * 11  # Rough approximation
        voltage_scaling_factor = vref * ac_voltage_ratio * self.voltage_calibration

        # Get the number of samples by checking the length of one of the sample buffers.
        for chan_num in range(1, 7):
            if f'ct{chan_num}' in samples.keys():
                num_samples = len(samples[f'ct{chan_num}'])
                break

        # Main loop
        for i in range(0, num_samples):
            for chan_num in range(0, len(ct_samples)):
                if ct_samples[chan_num] and v_samples[chan_num]:
                    ct = int(ct_samples[chan_num][i])
                    voltage = int(v_samples[chan_num][i])
                    sum_raw_current[chan_num] += ct
                    sum_raw_voltage[chan_num] += voltage
                    inst_power = ct * voltage
                    sum_inst_power[chan_num] += inst_power
                    squared_voltage = voltage * voltage
                    sum_squared_voltage[chan_num] += squared_voltage
                    sq_ct = ct * ct
                    sum_squared_current[chan_num] += sq_ct

        results = {}

        for chan_num in range(0, len(ct_samples)):
            if ct_samples[chan_num] and v_samples[chan_num]:
                avg_raw_current_ct1 = sum_raw_current[chan_num] / num_samples
                avg_raw_voltage_1 = sum_raw_voltage[chan_num] / num_samples
                real_power_1 = ((sum_inst_power[chan_num] / num_samples) - (
                        avg_raw_current_ct1 * avg_raw_voltage_1)) * ct_scaling_factors[chan_num] * voltage_scaling_factor
                mean_square_current_ct1 = sum_squared_current[chan_num] / num_samples
                mean_square_voltage_1 = sum_squared_voltage[chan_num] / num_samples
                rms_current_ct1 = sqrt(
                    mean_square_current_ct1 - (avg_raw_current_ct1 * avg_raw_current_ct1)) * ct_scaling_factors[chan_num]
                rms_voltage_1 = sqrt(
                    mean_square_voltage_1 - (avg_raw_voltage_1 * avg_raw_voltage_1)) * voltage_scaling_factor
                apparent_power_1 = rms_voltage_1 * rms_current_ct1
                try:
                    power_factor_1 = real_power_1 / apparent_power_1
                except ZeroDivisionError:
                    power_factor_1 = 0
                if self.config['current_transformers']['channel_1'].get('reversed'):
                    # RMS current is always positive, so the reverse check is done off the calculated real power.
                    if real_power_1 < 0:  # Make power positive (current is already positive)
                        real_power_1 = real_power_1 * -1
                        power_factor_1 = abs(power_factor_1)
                    else:  # Make current negative (real power is already negative)
                        rms_current_ct1 = rms_current_ct1 * -1

                results[chan_num+1] = {
                    'type': self.config['current_transformers'][f'channel_{chan_num+1}']['type'],
                    'power': real_power_1,
                    'current': rms_current_ct1,
                    'voltage': rms_voltage_1,
                    'pf': power_factor_1
                }

        # Grab the voltage from one of the enabled channels:
        results['voltage'] = results[self.enabled_channels[0]]['voltage']

        # Cutoff Threshold check
        for chan_num in self.enabled_channels:
            cutoff = float(self.config['current_transformers'][f'channel_{chan_num}']['watts_cutoff_threshold'])
            if cutoff != 0:
                if abs(results[chan_num]['power']) < cutoff:
                    results[chan_num]['power'] = 0
                    results[chan_num]['current'] = 0
                    results[chan_num]['pf'] = 0

        return results

    def cleanup(self, error_code=1):
        """Exits program after closing any existing connections or finishing active processes"""
        try:
            if self.spi:
                self.spi.close()
        except AttributeError:
            pass

        exit(error_code)

    @staticmethod
    def print_results(results, sample_rate):
        t = PrettyTable(['', 'ct1', 'ct2', 'ct3', 'ct4', 'ct5', 'ct6'])
        t.add_row(['Watts',
                   round(results[1]['power'] if results.get(1) else 0, 3),
                   round(results[2]['power'] if results.get(2) else 0, 3),
                   round(results[3]['power'] if results.get(3) else 0, 3),
                   round(results[4]['power'] if results.get(4) else 0, 3),
                   round(results[5]['power'] if results.get(5) else 0, 3),
                   round(results[6]['power'] if results.get(6) else 0, 3)])
        t.add_row(['Current',
                   round(results[1]['current'] if results.get(1) else 0, 3),
                   round(results[2]['current'] if results.get(2) else 0, 3),
                   round(results[3]['current'] if results.get(3) else 0, 3),
                   round(results[4]['current'] if results.get(4) else 0, 3),
                   round(results[5]['current'] if results.get(5) else 0, 3),
                   round(results[6]['current'] if results.get(6) else 0, 3)])
        t.add_row(['P.F.',
                   round(results[1]['pf'] if results.get(1) else 0, 3),
                   round(results[2]['pf'] if results.get(2) else 0, 3),
                   round(results[3]['pf'] if results.get(3) else 0, 3),
                   round(results[4]['pf'] if results.get(4) else 0, 3),
                   round(results[5]['pf'] if results.get(5) else 0, 3),
                   round(results[6]['pf'] if results.get(6) else 0, 3)])
        t.add_row(['Voltage', round(results['voltage'], 3), '', '', '', '', ''])
        t.add_row(['Sample Rate', sample_rate, 'kSPS', '', '', '', ''])
        s = t.get_string()
        logger.info(f"\n{s}")
