import requests

from utils import load_config


class TelegramBot():

    def __init__(self):
        self.config()
        self.url_template = ""

    def config(self):
        config = load_config()
        config = config["telegram"]
        token = config["token"]
        chat_id = config["chat_id"]
        self.url_template = f"https://api.telegram.org/bot{token}/sendMessage?chat_id={chat_id}"

    def send_message(self, message):
        message_payload = self.url_template + f"&text={message}"
        response = requests.get(message_payload).json()
        return response


def get_chat_id():
    config = load_config()
    config = config["telegram"]
    token = config["token"]
    url = f"https://api.telegram.org/bot{token}/getUpdates"
    return requests.get(url).json()


if __name__ == '__main__':
    print(get_chat_id())
