import argparse
import logging
import sys
import time

from energy_monitor import EnergyMonitor
from database.redis_utils import RedisManager
from database.mysql_utils import SqlManager

import random

from monitoring.telegram_bot import TelegramBot


def check_irregularity(old_avg, new_avg, threshold, label, bot: TelegramBot):
    if old_avg != 0:
        percentage_change = abs((new_avg - old_avg) / old_avg * 100)
        if percentage_change > threshold:
            bot.send_message(f"Alert: {label} has changed by {percentage_change:.2f}%")
    return new_avg


def driver(energy_monitor):
    """ Starts the main power monitor loop. """
    sql_manager = SqlManager()
    sql_manager.connect()
    redis_manager = RedisManager()
    redis_manager.connect()

    telegram_bot = TelegramBot()
    telegram_bot.config()

    logger.info("... Starting Raspberry Pi Power Monitor")
    logger.info("Press Ctrl-c to quit...")

    # Store values for each polling cycle
    production_values = dict(power=[], pf=[], current=[])
    home_consumption_values = dict(power=[], pf=[], current=[])
    net_values = dict(power=[], current=[])
    rms_voltages = []
    num_samples = 1000

    # Counter for averaging function
    i = 0
    start_time = time.time()
    current_setting = 1
    setting_time_seconds = 34
    # Get the expected sample count for the current configuration.
    samples = energy_monitor.collect_data(num_samples)
    sample_count = sum([len(samples[x]) for x in samples.keys() if type(samples[x]) == list])

    samples = []
    sample_count = 1

    # Moving averages
    moving_avg_production_power = 0
    moving_avg_home_power = 0
    moving_avg_net_power = 0

    # Main loop
    while True:
        try:
            board_voltage = energy_monitor.get_board_voltage()
            samples = energy_monitor.collect_data(num_samples)
            poll_time = samples['time']
            duration = samples['duration']
            duration = 1
            sample_rate = round((sample_count / duration) / num_samples, 2)

            # Single poll results
            results = energy_monitor.calculate_power(samples, board_voltage)

            production_power = 0
            production_current = 0
            production_pf = 0

            home_consumption_power = 0
            home_consumption_current = 0

            # Threshold for detecting irregularities (in percentage)
            threshold = 10

            # Set the RMS voltage using one of the calculated voltages.
            voltage = results[energy_monitor.enabled_channels[0]]['voltage']

            # Sum all production channels
            for chan_num in energy_monitor.production_channels:
                production_power += results[chan_num]['power']
                production_current += results[chan_num]['current']

            # Sum all consumption channels
            for chan_num in energy_monitor.consumption_channels:
                home_consumption_power += results[chan_num]['power']
                home_consumption_current += results[chan_num]['current']

            net_power = home_consumption_power - production_power
            net_current = home_consumption_current - production_current

            if net_power < 0:
                current_status = "Producing"
            else:
                current_status = "Consuming"

            # State of teh grid during the current poll
            redis_manager.redis_conn.set('current_status', current_status)

            # Don't store every interval to the database and instead send directly to dashboard via redis cache
            for chan_num in energy_monitor.enabled_channels:
                channel_type = 'consuming'
                if chan_num in energy_monitor.production_channels:
                    channel_type = 'producing'

                channel_data = {
                    'type': channel_type,
                    'power': results[chan_num]['power'],
                    'current': results[chan_num]['current'],
                    'pf': results[chan_num]['pf'],
                    'voltage': results[chan_num]['voltage'],
                }
                redis_manager.set_channel_data(chan_num, channel_data)

            # Store data in batches in DB
            if i < 10:
                production_values['power'].append(production_power)
                production_values['current'].append(production_current)
                production_values['pf'].append(production_pf)

                home_consumption_values['power'].append(home_consumption_power)
                home_consumption_values['current'].append(home_consumption_current)

                net_values['power'].append(net_power)
                net_values['current'].append(net_current)

                rms_voltages.append(voltage)
                i += 1
                time.sleep(1)
            else:
                # Calculate Averages
                avg_production_power = sum(production_values['power']) / i
                avg_production_current = sum(production_values['current']) / i
                avg_production_pf = sum(production_values['pf']) / i
                avg_home_power = sum(home_consumption_values['power']) / i
                avg_home_current = sum(home_consumption_values['current']) / i
                avg_net_power = sum(net_values['power']) / i
                avg_net_current = sum(net_values['current']) / i
                avg_voltage = sum(rms_voltages) / i

                moving_avg_production_power = check_irregularity(moving_avg_production_power, avg_production_power,
                                                                 threshold, "Production", telegram_bot)
                moving_avg_home_power = check_irregularity(moving_avg_home_power, avg_home_power, threshold,
                                                           "Consumption", telegram_bot)
                moving_avg_net_power = check_irregularity(moving_avg_net_power, avg_net_power, threshold, "Net Power",
                                                          telegram_bot)

                # Store aggregate values to persistent DB
                cursor = sql_manager.conn.cursor()
                sql_query = f"""
                    INSERT INTO readings 
                    (avg_production_power, avg_production_current, avg_production_pf, avg_home_power, avg_home_current, avg_net_power, avg_net_current, avg_voltage) 
                    VALUES 
                    (%s, %s, %s, %s, %s, %s, %s, %s)
                """
                values = (
                    avg_production_power,
                    avg_production_current,
                    avg_production_pf,
                    avg_home_power,
                    avg_home_current,
                    avg_net_power,
                    avg_net_current,
                    avg_voltage
                )
                cursor.execute(sql_query, values)
                sql_manager.conn.commit()
                cursor.close()

                # Reset the values and counter
                production_values = dict(power=[], pf=[], current=[])
                home_consumption_values = dict(power=[], pf=[], current=[])
                net_values = dict(power=[], current=[])
                rms_voltages = []
                i = 0

                if energy_monitor.terminal_mode:
                    results['voltage'] = voltage
                    energy_monitor.print_results(results, sample_rate)

        except KeyboardInterrupt:
            redis_manager.disconnect()
            sql_manager.disconnect()
            energy_monitor.cleanup(0)


if __name__ == '__main__':
    # Logging Config
    logger = logging.getLogger('energy-monitor')
    logger.setLevel(logging.INFO)
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s : %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch_formatter = logging.Formatter('%(levelname)s : %(message)s')
    ch.setFormatter(ch_formatter)
    logger.addHandler(ch)

    parser = argparse.ArgumentParser(description='Power Monitor CLI Interface')
    parser.add_argument('--mode', type=str, help="Operating Mode. Defaults to 'terminal' if not specified.",
                        default='terminal', required=False, choices=['db', 'terminal'])
    parser.add_argument('-v', '--verbose', help='Increases verbosity of program output.', action='store_true')
    args = parser.parse_args()

    if args.verbose:
        ch.setLevel(logging.DEBUG)
        logger.setLevel(logging.DEBUG)
        logger.debug("Verbose logs output enabled.")

    em = EnergyMonitor()

    if args.mode == 'terminal':
        em.terminal_mode = True
        logger.debug("Enabled terminal mode.")

    driver(em)
