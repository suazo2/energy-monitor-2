import pandas as pd
from dash import Dash, html, dcc, Output, Input

from database.mysql_utils import SqlManager

from database.redis_utils import RedisManager

app = Dash(__name__, external_stylesheets=['style.css'])

app.layout = html.Div(
    children=[
        html.Div(className="header", children="Energy Dashboard"),
        html.Div(
            className="container",
            children=[
                dcc.Interval(id='channel-interval-component', interval=1 * 1000, n_intervals=0),  # Update every 1 second
                html.Div(className="widget widget1", children=[
                    html.Div(className="widget-title", children="Channel 1"),
                    html.Div(className="widget-content widget-with-input", children=[
                        html.H4(id="channel-1-output", children="------"),
                    ]),
                ]),
                html.Div(className="widget widget2", children=[
                    html.Div(className="widget-title", children="Channel 2"),
                    html.Div(className="widget-content widget-with-input", children=[
                        html.H4(id="channel-2-output", children="------"),
                    ]),
                ]),
                html.Div(className="widget widget3", children=[
                    html.Div(className="widget-title", children="Channel 3"),
                    html.Div(className="widget-content widget-with-input", children=[
                        html.H4(id="channel-3-output", children="------"),
                    ]),
                ]),
                html.Div(className="widget widget4", children=[
                    html.Div(className="widget-title", children="Channel 4"),
                    html.Div(className="widget-content widget-with-input", children=[
                        html.H4(id="channel-4-output", children="------"),
                    ]),
                ]),
                html.Div(className="widget widget5", children=[
                    html.Div(className="widget-title", children="Channel 5"),
                    html.Div(className="widget-content widget-with-input", children=[
                        html.H4(id="channel-5-output", children="------"),
                    ]),
                ]),
                html.Div(className="widget widget6", children=[
                    html.Div(className="widget-title", children="Channel 6"),
                    html.Div(className="widget-content widget-with-input", children=[
                        html.H4(id="channel-6-output", children="------"),
                    ]),
                ]),
                html.Div(className="widget widget7", children=[
                    html.Div(className="widget-title", children="Channel 7"),
                    html.Div(className="widget-content widget-with-input", children=[
                        dcc.Graph(id="chart-output"),
                        dcc.Interval(id='interval-component', interval=5 * 1000, n_intervals=0)
                        # Update every 10 seconds
                    ]),
                ]),
            ]
        )
    ]
)


# ----------------------------------- DB Queries -----------------------------------


def get_readings():
    manager = SqlManager()
    manager.connect()
    cursor = manager.conn.cursor()
    query = f"""
        SELECT * FROM readings ORDER BY timestamp DESC LIMIT 50;
    """
    cursor.execute(query)
    rows = cursor.fetchall()

    df = pd.DataFrame(rows,
                      columns=['id', 'timestamp', 'avg_production_power', 'avg_production_current', 'avg_production_pf',
                               'avg_home_power', 'avg_home_current', 'avg_net_power', 'avg_net_current', 'avg_voltage'])

    cursor.close()
    manager.disconnect()

    return df


# ----------------------------------- CALLBACKS -----------------------------------

def format_power_value(power, channel_type):
    prefix = "+" if channel_type == "producing" else "-"
    color = "green" if channel_type == "producing" else "red"
    return html.Span(f"{prefix}{abs(power):.2f} W", style={"color": color})


@app.callback(
    [
        Output("channel-1-output", "children"),
        Output("channel-2-output", "children"),
        Output("channel-3-output", "children"),
        Output("channel-4-output", "children"),
        Output("channel-5-output", "children"),
        Output("channel-6-output", "children"),
    ],
    Input('channel-interval-component', 'n_intervals')
)
def update_channel_widgets(n_intervals):
    # Replace 'redis_manager' with your RedisManager instance
    redis_manager = RedisManager()
    redis_manager.connect()
    channels = [redis_manager.get_channel_data(i) for i in range(1, 7)]

    # Format the power value and style for each channel
    print(channels)
    formatted_channels = [format_power_value(channel['power'], channel['type']) if channel else "-----" for channel in channels]

    return formatted_channels


@app.callback(Output('chart-output', 'figure'), [Input('interval-component', 'n_intervals')])
def update_live_graph(n):
    df = get_readings()

    fig = {
        'data': [
            {
                'x': df['timestamp'],
                'y': df['avg_net_power'],
                'type': 'bar',
                'name': 'Net Power',
                'marker': {
                    'color': df['avg_net_power'],
                    'colorscale': [[0, 'green'], [1, 'red']],
                    'cmin': -max(abs(df['avg_net_power'].min()), df['avg_net_power'].max()),
                    'cmax': max(abs(df['avg_net_power'].min()), df['avg_net_power'].max()),
                    'showscale': False
                }
            }
        ],
        'layout': {
            'xaxis': {
                'title': 'Timestamp',
                'tickformat': '%M:%S'
            },
            'yaxis': {'title': 'Net Power (W)'},
            'barmode': 'relative'
        }
    }
    return fig


if __name__ == "__main__":
    app.run_server(debug=True, dev_tools_silence_routes_logging=False)
