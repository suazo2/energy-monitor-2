import mysql.connector

from utils import load_config


class SqlManager:

    def __init__(self):
        self.conn = None

    def connect(self):
        config = load_config()
        config = config["database"]
        self.conn = mysql.connector.connect(
            host=config.get("host", "localhost"),
            user=config.get("user"),
            password=config.get("password"),
            database=config.get("database")
        )

    def disconnect(self):
        self.conn.close()